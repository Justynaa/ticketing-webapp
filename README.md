# README #
Simple trouble ticketing system integrated with LDAP - study project for Computer Systems Administration

### Technologies and tools ###

* Java 8
* Spring MVC
* Spring Security 4
* Maven 3.3
* Tomcat 9