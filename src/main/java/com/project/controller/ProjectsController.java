package com.project.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.project.model.Project;
import com.project.service.ProjectService;

@Controller
public class ProjectsController {
	@Autowired
	ProjectService projectService;
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dateFormat.setLenient(false);
        dateFormat.setTimeZone(TimeZone.getDefault());
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }
	
	@RequestMapping(value = "/adminPanel/newProject", method = RequestMethod.GET)
	public String addProject(Model model){
        model.addAttribute("project", new Project());
		return "newProjectPage";
	}
	
	@RequestMapping(value = "/adminPanel/newProject", method = RequestMethod.POST)
	public String addProject(@Valid @ModelAttribute("project") Project project, BindingResult result, ModelMap model){
		if (result.hasErrors()) {		
			return "newProjectPage";
		}

		projectService.addProject(project);
		model.addAttribute("project", project);
		//sysout do usunieca
		System.out.println(project.getName()+ project.getStartDate() + project.getEndDate()+ project.getProjectLead() + project.getType());
		return "success";
	} 
	
	@RequestMapping(value="/adminPanel/listProjects", method=RequestMethod.GET)
	public String listProjects(Model model) {
	    List<Project> projectsList = projectService.listProjects();
	    model.addAttribute("projectsList", projectsList);
	    System.out.println("Is list empty? " + projectsList.isEmpty());
	    return "projectsPage";
	}
    @RequestMapping(path = "/adminPanel/editProject/{projectId}", method = RequestMethod.GET)
    public String editProject(Model model, @PathVariable(value = "projectId") Integer projectId) {
        model.addAttribute("project", projectService.findById(projectId));
        return "editProject";
    }    
    @RequestMapping(path = "/adminPanel/updateProject", method = RequestMethod.POST)
    public String updateProject(Project project) {
    	System.out.println(project.toString());
    	projectService.updateProject(project);
    	return "redirect:/adminPanel/listProjects";
    }	
}
