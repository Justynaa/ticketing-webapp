package com.project.controller;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.project.model.Ticket;
import com.project.service.TicketService;

@Controller
public class TicketsController {
	@Autowired
	TicketService ticketService;
	
	private Date ticketReportDate; //variable to keep report date unchanged

	@RequestMapping(value = "/tickets/newTicket", method = RequestMethod.GET)
	public String addTicket(Model model){
        model.addAttribute("ticket", new Ticket());
	    
        Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
        Map<String, Boolean> roles = getRoles(authentication);
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
        return "newTicketPage";
	}
	@RequestMapping(value = "/tickets/newTicket", method = RequestMethod.POST)
	public String addTicket(@Valid @ModelAttribute("ticket") Ticket ticket, BindingResult result, ModelMap model){
		if (result.hasErrors()) {		
			return "newTicketPage";
		}
		//get reporter name from session and set it to Ticket object
		Authentication authentication = SecurityContextHolder.getContext().
				getAuthentication();
		String name = authentication.getName();
		ticket.setReporter(name);

		ticketService.addTicket(ticket);
		model.addAttribute("ticket", ticket);
		
		//add role in order to display propprt viev (go to admin panel or go to user panel button)
	    Map<String, Boolean> roles = getRoles(authentication);	
		if(roles.containsKey("ROLE_ADMIN")){
			model.addAttribute("currentUserRole", "ROLE_ADMIN");
		}
		else {
			model.addAttribute("currentUserRole", "ROLE_USER");
		}
		return "successTicketAdd";
	}
	
	@RequestMapping(value="/adminPanel/listTickets", method=RequestMethod.GET)
	public String listTickets(Model model) {
	    List<Ticket> ticketsList = ticketService.listTickets();
	    model.addAttribute("ticketsList", ticketsList);
	    System.out.println("Is list empty? " + ticketsList.isEmpty());
	    return "ticketsPage";
	}
	
    @RequestMapping(path = "/adminPanel/editTicket/{ticketId}", method = RequestMethod.GET)
    public String editTicket(Model model, @PathVariable(value = "ticketId") Integer ticketId) {
        Ticket ticket = ticketService.findById(ticketId);
    	model.addAttribute("ticket", ticket);
        this.ticketReportDate = ticket.getReportDate(); //save ticket report date
        
        return "editTicket";
    }    
    @RequestMapping(path = "/adminPanel/updateTicket", method = RequestMethod.POST)
    public String updateTicket(@ModelAttribute("ticket") @Valid Ticket ticket, BindingResult result) {
    	System.out.println(ticket.toString());
    	ticket.setReportDate(this.ticketReportDate);
    	ticketService.updateTicket(ticket); //assign initial ticket report date back
    	return "redirect:/adminPanel/listTickets";
    }
	
	
	
	//Method returns roles of session user
	private static HashMap<String, Boolean> getRoles (Authentication authentication){
	    Collection<GrantedAuthority> credentials = (Collection<GrantedAuthority>) authentication.getAuthorities();

		HashMap<String, Boolean> roles = new HashMap<>();

	    for (GrantedAuthority authority : credentials) {
	           roles.put(authority.getAuthority(), true);
	    }
	    return roles;
	}
}
