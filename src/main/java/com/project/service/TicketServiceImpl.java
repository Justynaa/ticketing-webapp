package com.project.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.TicketDAO;
import com.project.model.Ticket;

@Service
public class TicketServiceImpl implements TicketService{
	
	@Autowired
	TicketDAO ticketDAO;
	
	//@Secured({"ROLE_ADMIN", "ROLE_USER"})
	public void addTicket(Ticket ticket) {
		//setting default values for new ticket properties - status, resolution 
		//ticket status may be: New, Open, In Progess, Closed, Resolved
		ticket.setStatus("New");
		
		//ticket resolution may be: false, true
		ticket.setResolution(false);
		
		//assign current date
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setTimeZone(TimeZone.getDefault());
		Date date = new Date();
	    ticket.setReportDate(date);    
		
		ticketDAO.addTicket(ticket);
	}
	
	public List<Ticket> listTickets() {
		List<Ticket> ticketsList = ticketDAO.listTickets();
		return ticketsList;
	}
	
	//@Secured({"ROLE_ADMIN", "ROLE_USER"})
	public Ticket findById(Integer id) {
		Ticket ticket = ticketDAO.findById(id);
		return ticket;
	}
	
	public void updateTicket(Ticket ticket) {
		ticketDAO.updateTicket(ticket);
	}
}
