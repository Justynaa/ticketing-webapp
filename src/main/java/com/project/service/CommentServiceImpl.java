package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.CommentDAO;
import com.project.model.Comment;

@Service
public class CommentServiceImpl implements CommentService{

	@Autowired
	CommentDAO commentDAO;
	
	@Override
	public void addComment(Comment comment) {
		commentDAO.addComment(comment);
	}
	
	@Override
	public List<Comment> listComments(){
		return commentDAO.listComments();
	}
	
	@Override
	public Comment findById(Integer commentId){
		return commentDAO.findById(commentId);
	}
	
	@Override
	public void updateComment(Comment comment){
		commentDAO.updateComment(comment);
	}
}

