package com.project.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.project.dao.ProjectDAO;
import com.project.model.Project;

@Service
public class ProjectServiceImpl implements ProjectService{
	@Autowired
	ProjectDAO projectDAO;
	
	@Override
	//@Secured({"ROLE_ADMIN", "ROLE_USER"})
	public void addProject(Project project) {
		projectDAO.addProject(project);
	}
	
	@Override
	public List<Project> listProjects(){
		return projectDAO.listProjects();
	}
	
	@Override
	public Project findById(Integer projectId){
		return projectDAO.findById(projectId);
	}
	
	@Override
	public void updateProject(Project project){
		projectDAO.updateProject(project);
	}
}
