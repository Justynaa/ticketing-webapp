package com.project.service;

import java.util.List;

import com.project.model.Ticket;

public interface TicketService {
	public void addTicket(Ticket ticket);
	public List<Ticket> listTickets();
	public Ticket findById(Integer id);
	public void updateTicket(Ticket ticket);
}
