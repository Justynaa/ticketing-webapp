package com.project.service;

import java.util.List;

import com.project.model.Project;

public interface ProjectService {
	public void addProject(Project project);
	public List<Project> listProjects();
	public Project findById(Integer projectId);
	public void updateProject(Project project);
}
