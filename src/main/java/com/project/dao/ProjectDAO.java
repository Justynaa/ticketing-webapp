package com.project.dao;

import java.util.List;

import com.project.model.Project;

public interface ProjectDAO {
	public void addProject(Project project);
	public List<Project> listProjects();
	public Project findById(Integer id);
	public void updateProject(Project project);
}
