package com.project.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.project.model.Project;

@Repository
public class ProjectDAOImpl implements ProjectDAO {
	
	private DataSource dataSource;

	@PersistenceContext
	EntityManager entityManager;

	private List<Project> projectsList = null;
	private Project project = null;
	private String queryStr;
	private Connection conn = null;

	public ProjectDAOImpl() {

	}
	@Autowired	
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	@Transactional
	public void addProject(Project project) {
		try {
			conn = dataSource.getConnection();
			entityManager.persist(project);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}		
		
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<Project> listProjects() {
		try {
			conn = dataSource.getConnection();

			queryStr = "SELECT p FROM Project p";
			Query query = entityManager.createQuery(queryStr);
			projectsList = query.getResultList();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return projectsList;
	}

	@Override
	@Transactional
	public Project findById(Integer projectId) {
		project = null;

		try {
			conn = dataSource.getConnection();

			project = new Project();
			project = entityManager.find(Project.class, projectId);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return project;
	}

	@Override
	@Transactional
	public void updateProject(Project project) {
		try {
			conn = dataSource.getConnection();
			//entityManager.persist(project); //error while updating project data (because id is set)
			entityManager.merge(project);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
