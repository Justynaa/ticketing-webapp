package com.project.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.project.model.Ticket;

@Repository
public class TicketDAOImpl implements TicketDAO {

	private DataSource dataSource;

	@PersistenceContext
	EntityManager entityManager;

	private List<Ticket> ticketsList = null;
	private Ticket ticket = null;
	private String queryStr;
	private Connection conn = null;

	public TicketDAOImpl() {

	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	@Transactional
	public void addTicket(Ticket ticket) {
		try {
			conn = dataSource.getConnection();
			entityManager.persist(ticket);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}		
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Ticket> listTickets() {
		try {
			conn = dataSource.getConnection();

			queryStr = "SELECT t FROM Ticket t";
			Query query = entityManager.createQuery(queryStr);
			ticketsList = query.getResultList();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ticketsList;
	}

	@Transactional
	public Ticket findById(Integer ticketId) {
		ticket = null;

		try {
			conn = dataSource.getConnection();

			ticket = new Ticket();
			ticket = entityManager.find(Ticket.class, ticketId);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ticket;
	}

	@Transactional
	public void updateTicket(Ticket ticket) {
		try {
			conn = dataSource.getConnection();
			entityManager.merge(ticket);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
