package com.project.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.project.model.Comment;

@Repository
public class CommentDAOImpl implements CommentDAO{

	@Autowired
	private DataSource dataSource;

	@PersistenceContext
	EntityManager entityManager;

	private List<Comment> commentsList = null;
	private Comment comment = null;
	private String queryStr;
	private Connection conn = null;

	public CommentDAOImpl() {

	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	@Transactional
	public void addComment(Comment comment) {
		try {
			conn = dataSource.getConnection();
			entityManager.persist(comment);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {}
			}
		}		
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Comment> listComments() {
		try {
			conn = dataSource.getConnection();

			queryStr = "SELECT c FROM Comment c";
			Query query = entityManager.createQuery(queryStr);
			commentsList = query.getResultList();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return commentsList;
	}

	@Transactional
	public Comment findById(Integer commentId) {
		comment = null;

		try {
			conn = dataSource.getConnection();

			comment = new Comment();
			comment = entityManager.find(Comment.class, commentId);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return comment;
	}

	@Transactional
	public void updateComment(Comment comment) {
		try {
			conn = dataSource.getConnection();
			entityManager.merge(comment);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
