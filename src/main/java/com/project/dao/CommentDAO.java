package com.project.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.project.model.Comment;

@Repository
public interface CommentDAO {
	
	public void addComment(Comment comment);
	public List<Comment> listComments();
	public Comment findById(Integer id);
	public void updateComment(Comment comment);
	
}
